#!/bin/bash

pkgs="wget tar openjdk-8-jdk"

echo "Installing essential tools..."
apt-get -y install $pkgs
if [ $? -ne 0 ]; then
  echo "Failed to install tools."
  exit 1
fi

# set profiles
# java
ETC_PROFILE_D=/etc/profile.d
echo "Installing java profile."
javacmdpath=`which java`
java_home=`readlink -f $javacmdpath | sed "s/^\(.*\)\/bin\/java$/\1/"`
JAVA_PROFILE=$ETC_PROFILE_D/java.sh
touch $JAVA_PROFILE
chmod a+x $JAVA_PROFILE
echo "export JAVA_HOME=$java_home" >> $JAVA_PROFILE
echo 'export PATH=$PATH:$JAVA_HOME/bin' >> $JAVA_PROFILE

# color-prompt
echo "Installing color-prompt profile."
template=templates/color-prompt.sh
color_prompt=/etc/profile.d/color-prompt.sh
if [ "x$ENV_PROFILE_TESTENV" = "xno" ]; then
  sed "s/__hostname_prefix__/p" $template > $color_prompt
else
  hostnameprefix=`hostname | sed "s/^\(.*\).$/\1/"`
  sed "s/__hostname_prefix__/$hostnameprefix/" $template > $color_prompt
fi  

. /etc/profile

# setup ssh welcome message
echo "Installing SSH welcome message."
template=templates/99-welcome
cp $template /etc/update-motd.d

exit 0
