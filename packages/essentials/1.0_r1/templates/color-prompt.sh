#!/bin/bash
# customize prompt color

Se="\["
Ee="\]"
Col="\e["
Rst="\e[0m"

hostname_prefix=__hostname_prefix__
hostname_number=`hostname | sed "s/$hostname_prefix//g"`

RED_BOLD="1;31m"

color="0;3`expr $hostname_number + 1`m"
if [ $color = "0;34m" ]; then
  color="0;36m"
fi
if [ `whoami` = "root" ]; then
  PS1="$Se$Col$RED_BOLD$Ee\u$Se$Rst$Ee@$Se$Col$color$Ee\h$Se$Rst$Ee:\w\\$ "
else
  PS1="\u@$Se$Col$color$Ee\h$Se$Rst$Ee:\w\\$ "
fi
