#!/bin/bash

BASE_NAME=`cd ..; pwd | sed "s/^.*\/\([^\/]*\)$/\1/"`
TOOL_HOME=/usr/local/etc/setup-node
PACKAGE_PREFIX=/usr/local
PACKAGE_HOME=$PACKAGE_PREFIX/$BASE_NAME
LOGDATA_PREFIX=/var/opt
LOG_DIR=$LOGDATA_PREFIX/$BASE_NAME/log
DATA_DIR=$LOGDATA_PREFIX/$BASE_NAME/data
PACKAGE_SRC_DIR=$BASE_NAME
TEMPLATES_DIR=templates
SERVICE_SCRIPT=$TEMPLATES_DIR/$BASE_NAME.service
PKG_USER=$BASE_NAME
PKG_GROUP=$BASE_NAME
USER_HOME_PREFIX=/home
version=`pwd | sed "s/^.*\/\([^_]*\)_[^_]*$/\1/"`

# import common utility
. $TOOL_HOME/util/*.sh

# custome environment variables
CONFIG_FILES="/etc/systemd/system/docker.service.d/http-proxy.conf /etc/systemd/system/docker.service.d/start-options.conf"

remove_user_group() {
  :
}

remove_files() {
  rm -f $CONFIG_FILES > /dev/null 2>&1
  echo "Purge docker package..."
  apt-get -y purge docker-engine
}

remove_log_directory() {
  :
}

remove_data_directory() {
if [ -d $DATA_DIR ]; then
  echo "Removing data directory '$DATA_DIR'..."
  rm -fr $DATA_DIR
fi
}

remove_custom_configuration() {
# add custom configuration
  :
}

unregister_service() {
echo "Unregistering service..."
service $BASE_NAME stop > /dev/null 2>&1
systemctl disable $BASE_NAME > /dev/null 2>&1
INIT_FILE=/etc/init.d/$BASE_NAME
rm -f $INIT_FILE
}

# Start
echo "Start to uninstall $BASE_NAME $version."
unregister_service
remove_custom_configuration
remove_data_directory
remove_log_directory
remove_files
remove_user_group

exit 0
