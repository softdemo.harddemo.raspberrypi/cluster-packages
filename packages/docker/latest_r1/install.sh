#!/bin/bash

BASE_NAME=`cd ..; pwd | sed "s/^.*\/\([^\/]*\)$/\1/"`
TOOL_HOME=/usr/local/etc/setup-node
PACKAGE_PREFIX=/usr/local
PACKAGE_HOME=$PACKAGE_PREFIX/$BASE_NAME
LOGDATA_PREFIX=/var/opt
LOG_DIR=$LOGDATA_PREFIX/$BASE_NAME/log
DATA_DIR=$LOGDATA_PREFIX/$BASE_NAME
PACKAGE_SRC_DIR=$BASE_NAME
TEMPLATES_DIR=templates
SERVICE_SCRIPT=$TEMPLATES_DIR/$BASE_NAME.service
PKG_USER=root
PKG_GROUP=root
USER_HOME_PREFIX=/home
version=`pwd | sed "s/^.*\/\([^_]*\)_[^_]*$/\1/"`

# import common utility
. $TOOL_HOME/util/*.sh

. /etc/profile

# custome environment variables
CONFIG_FILES="/etc/systemd/system/docker.service.d/http-proxy.conf /etc/systemd/system/docker.service.d/start-options.conf"

add_user_group() {
  :
}

copy_files() {
  echo "Copying and installing $BASE_NAME..."
  curl -sSL https://get.docker.com/ | sh
  if [ $? -ne 0 ]; then
    error_exit "Failed to install $BASE_NAME."
  fi
}

create_log_directory() {
  :
}

create_data_directory() {
echo "Making data directory..."
mkdir -p $DATA_DIR
if [ $? -ne 0 ]; then
  echo "*** ERROR : Unable to create directory '$DATA_DIR'"
  exit 1
fi
if [ $? -ne 0 ]; then
  echo "*** ERROR : Unable to chown to $DATA_DIR."
  exit 1
fi
}

setup_templates() {
# add custome setup template files
# set http proxy
proxy_conf=templates/http-proxy.conf
hasproxy=no
rm -f $proxy_conf > /dev/null 2>&1
if [ "x$http_proxy" != "x" ]; then
  hasproxy=yes
  touch $proxy_conf
  echo "[Service]" >> $proxy_conf
  temp=`grep 'master[0-9][0-9]*' /etc/hosts | sed "s/^\([0-9\.]*\).*/\1,/"`
  master_ips=`echo $temp | sed 's/ //g'`
  echo 'Environment="HTTP_PROXY='$http_proxy'" NO_PROXY="'$master_ips'localhost"' >> $proxy_conf
fi
option_conf=templates/start-options.conf
rm -f $option_conf > /dev/null 2>&1
master1_ip=`grep 'master1$' /etc/hosts | sed "s/^\([0-9\.]*\).*/\1/"`
if [ $? -ne 0 ]; then
  echo "'master1' definition not found in /etc/hosts."
  exit 1
fi
touch $option_conf
echo "[Service]" >> $option_conf
echo "ExecStart=" >> $option_conf
echo "ExecStart=/usr/bin/dockerd -H fd:// -g /var/opt/docker --insecure-registry ${master1_ip}:5000" >> $option_conf
}

setup_configuration() {
echo "Setup configuration files..."
mkdir -p /etc/systemd/system/docker.service.d
for conf in $CONFIG_FILES
do 
  name=`echo $conf | sed "s/^.*\/\([^\/]*\)$/\1/"` 
  srcfile=$TEMPLATES_DIR/$name
  destfile=$conf
  echo "Configuration : $destfile."
  cp $srcfile $destfile
  if [ $? -ne 0 ]; then
    echo "*** ERROR : Unable to copy config file '"$destfile"."
    exit 1
  fi
  chown $PKG_USER.$PKG_GROUP $destfile
done
}

custom_configuration() {
# add custom configuration
  :
}

register_service() {
# no need to register 
  systemctl daemon-reload
  service $BASE_NAME restart
}

# Start
echo "Start to install $BASE_NAME $version."
add_user_group
copy_files
create_log_directory
create_data_directory
setup_templates
setup_configuration
custom_configuration
register_service

exit 0
