#!/bin/bash

TOOL_HOME=/usr/local/etc/setup-node
pkgname=zookeeper
version=3.5.2-alpha
untardir=${pkgname}-$version
gzfile=$untardir.tar.gz
downloadurl="http://ftp.jaist.ac.jp/pub/apache/zookeeper/zookeeper-3.5.2-alpha/$gzfile"

# import common utility
. $TOOL_HOME/util/*.sh

echo "Stating to build ${pkgname}-${version}..."
echo "Install required tools..."
apt-get -y install wget tar
if [ $? -ne 0 ]; then
  echo "Failed to install required tools. Check message above."
  exit 1
fi

echo "Downloading $pkgname ..."
wget $downloadurl
if [ $? -ne 0 ]; then
  echo "Failed to download $pkgname distribution."
  exit 1
fi

echo "Unpack files..."
tar zxf $gzfile
rm -fr $gzfile > /dev/null 2>&1
mv $untardir $pkgname

exit 0
