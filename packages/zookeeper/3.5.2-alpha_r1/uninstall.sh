#!/bin/bash

BASE_NAME=`cd ..; pwd | sed "s/^.*\/\([^\/]*\)$/\1/"`
TOOL_HOME=/usr/local/etc/setup-node
PACKAGE_PREFIX=/usr/local
PACKAGE_HOME=$PACKAGE_PREFIX/$BASE_NAME
LOGDATA_PREFIX=/var/opt
LOG_DIR=$LOGDATA_PREFIX/$BASE_NAME/log
DATA_DIR=$LOGDATA_PREFIX/$BASE_NAME/data
PACKAGE_SRC_DIR=$BASE_NAME
TEMPLATES_DIR=templates
SERVICE_SCRIPT=$TEMPLATES_DIR/$BASE_NAME.service
PKG_USER=$BASE_NAME
PKG_GROUP=$BASE_NAME
USER_HOME_PREFIX=/home
version=`pwd | sed "s/^.*\/\([^_]*\)_[^_]*$/\1/"`

# import common utility
. $TOOL_HOME/util/*.sh

# custome environment variables
CONFIG_FILES="$PACKAGE_HOME/conf/zoo.cfg"

remove_user_group() {
  echo "Deleting user '$PKG_USER'..."
  userdel $PKG_USER
  echo "Deleting group '$PKG_GROUP'..."
  groupdel $PKG_GROUP
}

remove_files() {
echo "Removing package directory '$PACKAGE_HOME'..."
rm -fr $PACKAGE_HOME > /dev/null 2>&1
}

remove_log_directory() {
if [ -d $LOG_DIR ]; then
  echo "Removing log directory '$LOG_DIR'..."
  rm -fr $LOG_DIR
fi
}

remove_data_directory() {
if [ -d $DATA_DIR ]; then
  echo "Removing data directory '$DATA_DIR'..."
  rm -fr $DATA_DIR
fi
}

remove_custom_configuration() {
# add custom configuration
# setup myid file
echo "Removing '$DATA_DIR/myid'..."
rm -f $DATA_DIR/myid

}

unregister_service() {
echo "Unregistering service..."
service $BASE_NAME stop > /dev/null 2>&1
systemctl disable $BASE_NAME > /dev/null 2>&1
INIT_FILE=/etc/init.d/$BASE_NAME
rm -f $INIT_FILE
}

# Start
echo "Start to uninstall $BASE_NAME $version."
unregister_service
remove_custom_configuration
remove_data_directory
remove_log_directory
remove_files
remove_user_group

exit 0
