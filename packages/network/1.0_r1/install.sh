#!/bin/bash

echo "Installing network..."
echo "Copying : /etc/hosts"
cp templates/hosts /etc/hosts
if [ $? -ne 0 ]; then
  "Failed to copy hosts file."
fi

exit 0
