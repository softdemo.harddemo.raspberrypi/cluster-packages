#!/bin/bash

# color setting
EDCOL="\033[0m"
STCOL_RED="\033[0;31m"
STCOL_BLU="\033[0;34m"
STCOL_YEL="\033[0;33m"
STCOL_GRN="\033[0;32m"
STCOL_SYN="\033[0;36m"

get_num_masters() {
  while [ 1 ]
  do
    echo -e -n "${STCOL_GRN}How many masters will be set up ?${EDCOL} [4] : "
    read nummasters
    if [ "x$nummasters" = "x" ]; then
      nummasters=4
    fi
    err="*** Must be entered integer value (1-254)."
    chkval=`expr $nummasters + 1`
    if [ $? -ge 2 ]; then
      echo $err
    else
      if [ $chkval -eq 1 -o $chkval -gt 255 ]; then
        echo $err
      else
        echo "Set up hosts file for $nummasters master nodes..."
        break
      fi
    fi
  done
  NUM_OF_MASTERS=$nummasters
}

write_hostsfile_header() {
  _outfile=$1
  # create header
  echo "127.0.0.1       localhost" >> $_outfile
  echo >> $_outfile
  echo "# The following lines are desirable for IPv6 capable hosts" >> $_outfile
  echo "::1     localhost ip6-localhost ip6-loopback" >> $_outfile
  echo "ff02::1 ip6-allnodes" >> $_outfile
  echo "ff02::2 ip6-allrouters" >> $_outfile
  echo >> $_outfile
}

setup_real_hostsfile() {
  write_hostsfile_header $hostsfile
  echo "# Cluster virtual IP address for keepalive" >> $hostsfile
  echo "172.31.0.1	gate.picluster	p0.n0.picluster" >> $hostsfile	
  ct=1
  while [ $ct -le $NUM_OF_MASTERS ]
  do
    echo >> $hostsfile
    echo "# cluster n$ct" >> $hostsfile
    echo "172.31.$ct.100	p0.n$ct.picluster      master$ct" >> $hostsfile
    echo "172.31.$ct.101	p1.n$ct.picluster" >> $hostsfile
    echo "172.31.$ct.102	p2.n$ct.picluster" >> $hostsfile
    echo "172.31.$ct.103	p3.n$ct.picluster" >> $hostsfile
    echo "172.31.$ct.104	p4.n$ct.picluster" >> $hostsfile
    ct=`expr $ct + 1`
  done
}

setup_test_hostsfile() {
  write_hostsfile_header $hostsfile
  hostprefix=`hostname | sed "s/^\(.*\)[0-9]$/\1/"`
  echo "# Cluster virtual IP address for keepalive" >> $hostsfile
  echo "192.168.7.10	${hostprefix}0" >> $hostsfile
  echo >> $hostsfile
  ct=1
  while [ $ct -le $NUM_OF_MASTERS ]
  do
    echo "192.168.7.1$ct	${hostprefix}$ct      master$ct" >> $hostsfile
    ct=`expr $ct + 1` 
  done
}


# setup hosts file
get_num_masters
hostsfile=templates/hosts
cat /dev/null > $hostsfile

if [ "$ENV_PROFILE_TESTENV" = "no" ]; then
  setup_real_hostsfile 
else
  setup_test_hostsfile
fi 
echo "Successfully finished to setup hosts file."

exit 0
