#!/bin/bash

TOOL_HOME=/usr/local/etc/setup-node

# import common utility
. $TOOL_HOME/util/*.sh

echo "Stating to build Marathon-1.4.1..."
echo "Install required tools..."
apt-get -y install wget tar
if [ $? -ne 0 ]; then
  echo "Failed to install required tools. Check message above."
  exit 1
fi

echo "Downloading Marathon ..."
wget http://downloads.mesosphere.com/marathon/v1.4.1/marathon-1.4.1.tgz
if [ $? -ne 0 ]; then
  echo "Failed to download marathon distribution."
  exit 1
fi

echo "Unpack files..."
tar zxf marathon-1.4.1.tgz
rm -fr marathon-1.4.1.tgz > /dev/null 2>&1
mv marathon-1.4.1 marathon

cd marathon

exit 0
