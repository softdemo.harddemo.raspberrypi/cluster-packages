#!/bin/bash

BASE_NAME=`cd ..; pwd | sed "s/^.*\/\([^\/]*\)$/\1/"`
TOOL_HOME=/usr/local/etc/setup-node
PACKAGE_PREFIX=/usr/local
PACKAGE_HOME=$PACKAGE_PREFIX/$BASE_NAME
LOGDATA_PREFIX=/var/opt
LOG_DIR=$LOGDATA_PREFIX/$BASE_NAME/log
DATA_DIR=$LOGDATA_PREFIX/$BASE_NAME/data
PACKAGE_SRC_DIR=$BASE_NAME
TEMPLATES_DIR=templates
SERVICE_SCRIPT=$TEMPLATES_DIR/$BASE_NAME.service
#PKG_USER=$BASE_NAME
PGK_USER=mesos
#PKG_GROUP=$BASE_NAME
PKG_GROUP=mesos
USER_HOME_PREFIX=/home
version=`pwd | sed "s/^.*\/\([^_]*\)_[^_]*$/\1/"`

# import common utility
. $TOOL_HOME/util/*.sh

# custome environment variables
CONFIG_FILES=

add_user_group() {
grep "^$PKG_GROUP:" /etc/group > /dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "Group '"$PKG_GROUP"' already exists."
else
  echo "Add user and group for $PKG_GROUP..."
  groupadd $PKG_GROUP
  useradd -g $PKG_GROUP -s /bin/bash -d $USER_HOME_PREFIX/$PKG_USER -m $PKG_USER
fi
}

copy_files() {
rm -fr $PACKAGE_HOME > /dev/null 2>&1
echo "Copying files..."
cp -r ./$PACKAGE_SRC_DIR $PACKAGE_PREFIX
if [ $? -ne 0 ]; then
  echo "*** ERROR : Unable to copy files, installation terminated."
  exit 1
fi
chown -R $PKG_USER.$PKG_GROUP $PACKAGE_PREFIX/$PACKAGE_SRC_DIR
}

create_log_directory() {
echo "Making log directory..."
mkdir -p $LOG_DIR
if [ $? -ne 0 ]; then
  echo "*** ERROR : Unable to create directory '$LOG_DIR'"
  exit 1
fi
chown $PKG_USER.$PKG_GROUP $LOG_DIR
if [ $? -ne 0 ]; then
  echo "*** ERROR : Unable to chown to $LOG_DIR."
  exit 1
fi
}

create_data_directory() {
echo "Making data directory..."
mkdir -p $DATA_DIR
if [ $? -ne 0 ]; then
  echo "*** ERROR : Unable to create directory '$DATA_DIR'"
  exit 1
fi
chown $PKG_USER.$PKG_GROUP $DATA_DIR
if [ $? -ne 0 ]; then
  echo "*** ERROR : Unable to chown to $DATA_DIR."
  exit 1
fi
}

setup_templates() {
# add custome setup template files
  :
}

setup_configuration() {
echo "Setup configuration files..."
for conf in $CONFIG_FILES
do 
  name=`echo $conf | sed "s/^.*\/\([^\/]*\)$/\1/"` 
  srcfile=$TEMPLATES_DIR/$name
  destfile=$conf
  echo "Configuration : $destfile."
  cp $srcfile $destfile
  if [ $? -ne 0 ]; then
    echo "*** ERROR : Unable to copy config file '"$destfile"."
    exit 1
  fi
  chown $PKG_USER.$PKG_GROUP $destfile
done
}

custom_configuration() {
# add custom configuration
  :
}

register_service() {
echo "Registering service..."
systemctl disable $BASE_NAME > /dev/null 2>&1
INIT_FILE=/etc/init.d/$BASE_NAME
cp $SERVICE_SCRIPT $INIT_FILE
chmod a+x $INIT_FILE
systemctl enable $BASE_NAME
if [ $? -ne 0 ]; then
  echo "*** ERROR : Unable to register service '$BASE_NAME'"
  exit 1
fi
service $BASE_NAME stop
systemctl disable $BASE_NAME
}

# Start
echo "Start to install $BASE_NAME $version."
add_user_group
copy_files
create_log_directory
create_data_directory
setup_templates
setup_configuration
custom_configuration
register_service

exit 0
