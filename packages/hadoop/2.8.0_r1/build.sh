#!/bin/bash

TOOL_HOME=/usr/local/etc/setup-node
pkgname=hadoop
version=2.8.0
untardir=hadoop-2.8.0-src
gzfile=hadoop-2.8.0-src.tar.gz
downloadurl="http://ftp.jaist.ac.jp/pub/apache/hadoop/common/hadoop-2.8.0/hadoop-2.8.0-src.tar.gz"

# import common utility
. $TOOL_HOME/util/*.sh

rm -fr $package > /dev/null 2>&1
echo "Stating to build ${pkgname}-${version}..."
echo "Install required tools..."
if [ "x`protoc --version`" = "xlibprotoc 2.5.0" ]; then 
  echo "libprotoc 2.5.0 already installed."
else 
  echo "Building libprotoc 2.5.0..."
  wget https://github.com/google/protobuf/releases/download/v2.5.0/protobuf-2.5.0.tar.gz
  tar zxf protobuf-2.5.0.tar.gz
  rm protobuf-2.5.0.tar.gz
  cd protobuf-2.5.0
  ./autogen.sh
  ./configure --prefix=/usr
  make
  if [ $? -ne 0 ]; then
    echo "Failed to build protobuf-2.5.0."
    exit 1
  fi
  make install
  protoc --version
  if [ $? -ne 0 ]; then
    echo "Failed to install protobuf-2.5.0."
    exit 1
  fi
  cd ..
  rm -fr protobuf-2.5.0
fi

echo "Downloading $pkgname ..."
wget $downloadurl
if [ $? -ne 0 ]; then
  echo "Failed to download $pkgname source."
  exit 1
fi

echo "Unpack files..."
tar zxf $gzfile
rm -fr $gzfile $pkgname > /dev/null 2>&1
mv $untardir $pkgname

# make hadoop
echo "Compiling ${pkgname}-${version} ..."
proxy_opt=""
if [ "x$http_proxy" != "x" ]; then
  proxy_host=`echo $http_proxy | sed "s/http[s]*:\/\/\(.*\):.*/\1/"`
  proxy_port=`echo $http_proxy | sed "s/http[s]*:\/\/.*:\([0-9][0-9]*\).*/\1/"`
  proxy_shost=`echo $https_proxy | sed "s/http[s]*:\/\/\(.*\):.*/\1/"`
  proxy_sport=`echo $https_proxy | sed "s/http[s]*:\/\/.*:\([0-9][0-9]*\).*/\1/"`
  proxy_opt="-Dhttp.proxyHost=$proxy_host -Dhttp.proxyPort=$proxy_port"
  mvn_setting=templates/settings.xml
  template=$mvn_setting.template
  sed "s/HTTP_PROXY_HOST/${proxy_host}/" $template |	\
  sed "s/HTTP_PROXY_PORT/${proxy_port}/"  	|	\
  sed "s/HTTPS_PROXY_HOST/${proxy_shost}/" 	| 	\
  sed "s/HTTPS_PROXY_PORT/${proxy_sport}/" 	> $mvn_setting
  mkdir -p $HOME/.m2 > /dev/null 2>&1
  cp $mvn_setting $HOME/.m2/
fi
cd $pkgname
echo "JAVA_HOME=$JAVA_HOME"
echo "mvn package -Pdist,native -DskipTests -Dtar $proxy_opt"
JAVA_HOME=`echo $JAVA_HOME | sed "s/^\(.*\)\/jre/\1/"`
JAVA_HOME=$JAVA_HOME mvn -X -e package -Pdist,native -DskipTests -Dtar $proxy_opt
distfile=./hadoop-dist/target/hadoop-2.7.3/hadoop-2.7.3.tar.gz
if [ $? -ne 0 -o ! -f $distfile ]; then
  echo "Failed to compile ${pkgname}-${version} ."
  exit 1
fi
mv $distfile ..





exit 0
